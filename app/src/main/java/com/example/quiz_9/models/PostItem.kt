package com.example.quiz_9.models


import com.squareup.moshi.Json

data class PostItem(
    val cover: String?,
    val liked: Boolean?,
    val price: String?,
    val title: String?
)