package com.example.quiz_9

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.quiz_9.models.Post
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class PostViewModel @Inject constructor(
    private val postRepository: PostRepository
) : ViewModel() {

    private val _postResponse = MutableSharedFlow<Resource<Post>>()
    val postResponse: SharedFlow<Resource<Post>>
        get() = _postResponse

    fun loadUsers() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val resource = postRepository.getUsers()
                _postResponse.emit(resource)
            }
        }
    }
}

