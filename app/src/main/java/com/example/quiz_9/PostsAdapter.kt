package com.example.quiz_9

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.quiz_9.databinding.LayoutPostBinding
import com.example.quiz_9.extensions.setImageUrl
import com.example.quiz_9.models.PostItem

class PostListAdapter  : ListAdapter<PostItem , PostListAdapter.ViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int)=
        ViewHolder(
            LayoutPostBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(private val binding: LayoutPostBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(post: PostItem) = with(binding){
            tvTitle.text = post.title
            imCover.setImageUrl(post.cover)
        }
    }

    class DiffCallback : DiffUtil.ItemCallback<PostItem>() {
        override fun areItemsTheSame(oldItem: PostItem, newItem: PostItem): Boolean =
            oldItem.title == newItem.title

        override fun areContentsTheSame(oldItem: PostItem, newItem: PostItem): Boolean =
            oldItem == newItem
    }
}