package com.example.quiz_9

import androidx.annotation.WorkerThread
import com.example.quiz_9.daos.PostDao
import com.example.quiz_9.room.Post
import kotlinx.coroutines.flow.Flow

class PostRoomRepository(private val postDao: PostDao) {

    val allPost: Flow<List<Post>> = postDao.getPosts()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(post: Post) {
        postDao.insert(post)
    }
}
