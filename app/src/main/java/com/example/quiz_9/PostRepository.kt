package com.example.quiz_9

import com.example.quiz_9.models.Post
import com.example.quiz_9.networking.ApiService
import javax.inject.Inject

class PostRepository @Inject constructor(
    private val api: ApiService
) {

    suspend fun getUsers(): Resource<Post> {
        return try {
            Resource.Loading(null)
            val response = api.getPostData()
            val result = response.body()
            if (response.isSuccessful && result != null)
                Resource.Success(result)
            else
                Resource.Error(message = response.message().toString())
        } catch (e: Exception) {
            Resource.Error(message = e.toString())
        }
    }

}
