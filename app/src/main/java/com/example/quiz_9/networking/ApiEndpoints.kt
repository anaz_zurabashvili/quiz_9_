package com.example.quiz_9.networking

object ApiEndpoints {
    const val BASE_URL = "https://run.mocky.io/v3/"
    const val POSTS = "05d71804-4628-4269-ac03-f86e9960a0bb"
}