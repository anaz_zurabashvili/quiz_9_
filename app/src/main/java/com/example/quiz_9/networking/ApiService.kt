package com.example.quiz_9.networking


import com.example.quiz_9.models.Post
import retrofit2.Response
import retrofit2.http.GET


interface ApiService {

    @GET(ApiEndpoints.POSTS)
    suspend fun getPostData(): Response<Post>


}


