package com.example.quiz_9

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.quiz_9.room.Post
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch


class PostRoomViewModel (
    private val postRepository: PostRoomRepository
) : ViewModel() {
    private val _postForm = MutableSharedFlow<Post>()
    val postForm: SharedFlow<Post> = _postForm

    // Using LiveData and caching what allWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    val allUsers: Flow<List<Post>> = PostRoomRepository.allPost

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insert(user: User) = viewModelScope.launch {
        userRepository.insert(user)
    }

    suspend fun userValidation(firstName: String , lastName: String) {
        when {
            firstName.isEmpty() -> {
                _userForm.emit(UserFormState(firstNameError = R.string.invalid_username))
            }
            firstName.isEmpty() -> {
                _userForm.emit(UserFormState(lastNameError = R.string.invalid_username))
            }
            else -> {
                _userForm.emit(UserFormState(isDataValid = true))
            }
        }
    }
}

