package com.example.quiz_9

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.quiz_9.base.BaseFragment
import com.example.quiz_9.databinding.FragmentPostsBinding


class PostsFragment: BaseFragment<FragmentPostsBinding>(FragmentPostsBinding::inflate) {
    private val viewModel: PostViewModel by viewModels()
    private lateinit var postAdapter: PostListAdapter
    override fun init() {
        observes()
        initRV()
        viewModel.loadUsers()
    }

    private fun observes() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.postResponse.collect {
//                when (postResponse) {
//                    is Resource.Loading -> {
//                    }
//                    is Resource.Success -> {
////                        binding.swipeStatementFragment.refreshing(false)
//                        searchAdapter.setData(postResponse.data!!)
//                    }
//                    is Resource.Error -> {
////                        binding.swipeStatementFragment.refreshing(true)
//                    }
//                }
            }
        }
    }

    private fun initRV() {
        binding.rvUsers.apply {
            postAdapter = PostListAdapter()
            adapter = postAdapter
            layoutManager =
                GridLayoutManager(view?.context , 2 , LinearLayoutManager.HORIZONTAL , false)
        }
    }
}