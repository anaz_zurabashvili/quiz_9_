package com.example.quiz_9.room

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "post")
data class Post (
    @ColumnInfo(name = "cover") val cover: String? ,
    @ColumnInfo(name = "liked") val liked: Boolean?,
    @ColumnInfo(name = "price") val price: Boolean?,
    @ColumnInfo(name = "title") val title: Boolean?,
)

